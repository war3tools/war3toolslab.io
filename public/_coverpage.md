<!-- _coverpage.md -->
<style>
.title {
    font-weight: bold;
    font-size: 50px;
    background-size: 400% 400%;
    background-image: linear-gradient(to right, #ff0000, #ff8000, #d83b01, #000000, #00d0ff, #9000ff,#ff008c);
    -webkit-background-clip: text;
    animation: text-color-gradient 10s ease infinite;
    color: transparent;
}
@keyframes text-color-gradient {
  0%{background-position:0% 50%}
  50%{background-position:100% 50%}
  100%{background-position:0% 50%}
}
</style>

<div class="title">魔兽争霸3改键SuWar3Tools</div>

> 永久访问地址： https://war3tools.gitlab.io

<p align="center">
  <img src="https://visitor-badge.laobi.icu/badge?page_id=war3tools.gitlab.io"/>&nbsp;&nbsp;&nbsp;&nbsp;<img src="https://hits.dwyl.com/war3tools/war3tools.gitlab.io.svg"/>
</p>

<p align="center">
  <img src="./favicon.ico"/>
</p>

[Gitlab](https://gitlab.com/war3tools/war3tools.gitlab.io)
[Getting Started](/README.md)
